package com.example.mymlkit;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.OutputConfiguration;
import android.hardware.camera2.params.SessionConfiguration;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.renderscript.Type;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.WindowManager;

import com.example.mymlkit.databinding.ActivityMainBinding;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Frame;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.google.gson.Gson;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ReadOnlyBufferException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executor;

public class MainActivity extends AppCompatActivity {

    private CameraDevice mCameraDevice;
    private CameraManager cameraManager;
    private CameraSource cameraSource;
    private ImageReader imageReader;
    private ActivityMainBinding activityMainBinding;
    private boolean stop = false;
    private static final int MID_VALUE = 128;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(activityMainBinding.getRoot());
        cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Gson gson = new Gson();
                        Log.e("max", "getDynamicLink:onSuccess    "+ gson.toJson(pendingDynamicLinkData));
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                        }


                        // Handle the deep link. For example, open the linked
                        // content, or apply promotional credit to the user's
                        // account.
                        // ...

                        // ...
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("max", "getDynamicLink:onFailure", e);
                    }
                });

        FirebaseVisionBarcodeDetectorOptions options =
                new FirebaseVisionBarcodeDetectorOptions.Builder()
                        .setBarcodeFormats(
                                FirebaseVisionBarcode.FORMAT_QR_CODE,
                                FirebaseVisionBarcode.FORMAT_AZTEC)
                        .build();


        if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
                == ConnectionResult.SUCCESS) {
            // The SafetyNet Attestation API is available.
            // The nonce should be at least 16 bytes in length.
// You must generate the value of API_KEY in the Google APIs dashboard.
            byte[] nonce = new byte[16];
            SecureRandom secureRandom = new SecureRandom();
            secureRandom.nextBytes(nonce);

            SafetyNet.getClient(this).attest("abcd1234abcd1234".getBytes(), "AIzaSyDQEWqW1L7lUVlrXtMdunSGexb5Htxd4aI")
                    .addOnSuccessListener(this,
                            new OnSuccessListener<SafetyNetApi.AttestationResponse>() {
                                @Override
                                public void onSuccess(SafetyNetApi.AttestationResponse response) {
                                    // Indicates communication with the service was successful.
                                    // Use response.getJwsResult() to get the result data.
//                                    Gson gson = new Gson();
//                                    Log.e("wtf","onSuccess  "+gson.toJson(response));
                                    Log.e("wtf",""+response.getJwsResult());
                                    try {
                                        VolleyRequest.send(MainActivity.this,response.getJwsResult());
                                    } catch (JSONException e) {
                                        Log.e("Wtf","volley error  "+e.toString());
                                        e.printStackTrace();
                                    }
                                }
                            })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // An error occurred while communicating with the service.
                            Log.e("wtf","onFailure  "+e.toString());
                            if (e instanceof ApiException) {
                                // An error with the Google Play services API contains some
                                // additional details.
                                ApiException apiException = (ApiException) e;
                                Log.e("wtf","  "+apiException.getMessage()+"   "+apiException.getStatus());
                                // You can retrieve the status code using the
                                // apiException.getStatusCode() method.
                            } else {
                                // A different, unknown type of error occurred.
                            }
                        }
                    });
        } else {

            Log.e("wtf","update  ");
            // Prompt user to update Google Play services.
        }

//
    }

    private void openCamera(SurfaceTexture surfaceTexture){
        try {
            for (String cameraId : cameraManager.getCameraIdList()) {
                CameraCharacteristics characteristics = cameraManager.getCameraCharacteristics(cameraId);
                //默认打开后置摄像头
                if (characteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT)
                    continue;
                //获取StreamConfigurationMap，它是管理摄像头支持的所有输出格式和尺寸
                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                //根据TextureView的尺寸设置预览尺寸
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                cameraManager.openCamera(cameraId, new CameraDevice.StateCallback() {
                    @Override
                    public void onOpened(@NonNull CameraDevice cameraDevice) {
                        mCameraDevice = cameraDevice;
                        try {
                            preview(cameraDevice,surfaceTexture);
//                            Log.e("Wtf","open");
                        } catch (CameraAccessException e) {
//                            Log.e("Wtf","opeen fail "+e.toString());
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onDisconnected(@NonNull CameraDevice cameraDevice) {

                    }

                    @Override
                    public void onError(@NonNull CameraDevice cameraDevice, int i) {

                    }
                }, new Handler(Looper.getMainLooper()));
                break;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void preview(CameraDevice cameraDevice,SurfaceTexture surfaceTexture) throws CameraAccessException {
        CaptureRequest.Builder builder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);

        imageReader = ImageReader.newInstance(720,1280,ImageFormat.YUV_420_888,2);
        imageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader imageReader) {
                Image img = imageReader.acquireLatestImage();
                if (img != null) {
                    Image.Plane[] planes = img.getPlanes();
                    if (planes[0].getBuffer() == null) {
                        return;
                    }

                    if(!stop) {
                        ocr(img, cameraDevice.getId());
                    } else {
                        img.close();
                    }
                }
            }

        },new Handler(getMainLooper()));
        surfaceTexture.setDefaultBufferSize(720, 1280);
        Surface surface = new Surface(surfaceTexture);
        builder.addTarget(surface) ;
        builder.addTarget(imageReader.getSurface());
        List<Surface> surfaceList = new ArrayList<>();
        surfaceList.add(surface);
        surfaceList.add(imageReader.getSurface());

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            mCameraDevice.createCaptureSession(surfaceList, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
//                    Log.e("Wtf", "cameraCaptureSession  " + cameraCaptureSession);

                    try {
                        cameraCaptureSession.capture(builder.build(), new CameraCaptureSession.CaptureCallback() {
                            @Override
                            public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp, long frameNumber) {
                                super.onCaptureStarted(session, request, timestamp, frameNumber);
                            }

                            @Override
                            public void onCaptureProgressed(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull CaptureResult partialResult) {
                                super.onCaptureProgressed(session, request, partialResult);
                            }

                            @Override
                            public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                                super.onCaptureCompleted(session, request, result);
//                                Log.e("Wtf", "onCaptureCompleted = ");

                                try {
                                    session.setRepeatingRequest(builder.build(), new CameraCaptureSession.CaptureCallback() {
                                        @Override
                                        public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                                            super.onCaptureCompleted(session, request, result);
//                                            Log.e("Wtf", "onCaptureCompleted");
                                        }
                                    }, new Handler(Looper.getMainLooper()));
                                } catch (CameraAccessException e) {
                                    e.printStackTrace();
                                }

                            }

                            @Override
                            public void onCaptureFailed(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull CaptureFailure failure) {
                                super.onCaptureFailed(session, request, failure);
//                                Log.e("Wtf", "onCaptureFailed = ");
                            }

                            @Override
                            public void onCaptureSequenceCompleted(@NonNull CameraCaptureSession session, int sequenceId, long frameNumber) {
                                super.onCaptureSequenceCompleted(session, sequenceId, frameNumber);
                            }

                            @Override
                            public void onCaptureSequenceAborted(@NonNull CameraCaptureSession session, int sequenceId) {
                                super.onCaptureSequenceAborted(session, sequenceId);
                            }

                            @Override
                            public void onCaptureBufferLost(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull Surface target, long frameNumber) {
                                super.onCaptureBufferLost(session, request, target, frameNumber);
                            }
                        }, new Handler(Looper.getMainLooper()));
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
//                        Log.e("Wtf", "e = " + e.toString());
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {

                }
            }, new Handler(Looper.getMainLooper()));
        } else {
            List<OutputConfiguration> outputConfigurations = new ArrayList<>();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                OutputConfiguration outputConfiguration = new OutputConfiguration(surface);
                outputConfigurations.add(outputConfiguration);
                OutputConfiguration imgOutputConfiguration = new OutputConfiguration(imageReader.getSurface());
                outputConfigurations.add(imgOutputConfiguration);
            }


            SessionConfiguration sessionConfiguration = new SessionConfiguration(SessionConfiguration.SESSION_REGULAR, outputConfigurations, new Executor() {
                @Override
                public void execute(Runnable runnable) {
                    runnable.run();
                }
            }, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    try {
                        cameraCaptureSession.setRepeatingRequest(builder.build(), new CameraCaptureSession.CaptureCallback() {
                            @Override
                            public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                                super.onCaptureCompleted(session, request, result);
//                                Log.e("Wtf", "onCaptureCompleted");
                            }
                        }, new Handler(Looper.getMainLooper()));
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
//                    Log.e("Wtf", "onConfigureFailed");
                }
            });
            cameraDevice.createCaptureSession(sessionConfiguration);
        }
    }

    private void mlKit(Bitmap myBitmap){
        FirebaseVisionBarcodeDetector detector = FirebaseVision.getInstance()
                .getVisionBarcodeDetector();

        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(myBitmap);
        detector.detectInImage(image)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionBarcode> barcodes) {
                        // Task completed successfully
                        // ...
                        for (FirebaseVisionBarcode barcode : barcodes) {
//                            Log.e("wtf","barcode = "+ barcode.getDisplayValue() + "    " + barcode.getRawValue());
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Task failed with an exception
                        // ...
//                        Log.e("Wtf",e.toString());
                    }
                });


    }

    private void ocr(Image img,String cameraId){
        stop = true;
        new Thread(()->{
            try {
                Bitmap myBitmap = null;
                myBitmap = imgToBitmap(img, img.getWidth(), img.getHeight());
                img.close();

                Matrix matrix = new Matrix();

                int oritation = 0;

                oritation = getJpegOrientation(cameraManager.getCameraCharacteristics(cameraId), getResources().getConfiguration().orientation);


//                Log.e("Wtf", "oritation  = " + oritation);
                matrix.postRotate(oritation);
                Bitmap rotatedBitmap;
                rotatedBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);


                Bitmap cropBitmap = Bitmap.createBitmap(rotatedBitmap, 0,
                        rotatedBitmap.getHeight() /5, rotatedBitmap.getWidth(), rotatedBitmap.getHeight() / 10);

                int light = getBright(cropBitmap);
                Bitmap changedBitmap = cropBitmap;
                if(light != 0) {
                    int changeLight = 128;
//                Log.e("Wtf ","br =  "+light);
                    changeLight = 16640 / light;

                    changedBitmap = getChangedBitmap(cropBitmap, 128, 0, changeLight);

                    light = getBright(changedBitmap);
//                Log.e("Wtf ","br change=  "+light);
                }

//                Bitmap twoBitBitmap = convertToBlackWhite(changedBitmap,changeLight);


//                savePic(twoBitBitmap);

                FirebaseVisionTextRecognizer detector = FirebaseVision.getInstance()
                        .getOnDeviceTextRecognizer();

                FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(changedBitmap);

                Task<FirebaseVisionText> textTask = detector.processImage(image);
                textTask.addOnSuccessListener(new OnSuccessListener<FirebaseVisionText>() {
                    @Override
                    public void onSuccess(FirebaseVisionText firebaseVisionText) {
//                        Log.e("tt", firebaseVisionText.getText());
                        activityMainBinding.ocrTextView.setText(firebaseVisionText.getText());
                        for (FirebaseVisionText.TextBlock textBlock : firebaseVisionText.getTextBlocks()) {
//                            Log.e("tt","textBlock  "+textBlock.getLines().size());
                            for (FirebaseVisionText.Line line : textBlock.getLines()) {
//                                Log.e("tt","line  "+line.getText());
//                                Log.e("tt","getElements  "+line.getElements().size());
                            }
                        }
                    }
                });

                Thread.sleep(500);
                stop = false;
            } catch (CameraAccessException e) {
                e.printStackTrace();
//                Log.e("Wtf","orit "+e.toString());
                stop = false;
            } catch (IOException e) {
                e.printStackTrace();
                stop =false;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

    }

    public int getBright(Bitmap bm) {
        if(bm == null) return -1;
        int width = bm.getWidth();
        int height = bm.getHeight();
        int r, g, b;
        int count = 0;
        int bright = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                count++;
                int localTemp = bm.getPixel(i, j);
                r = (localTemp | 0xff00ffff) >> 16 & 0x00ff;
                g = (localTemp | 0xffff00ff) >> 8 & 0x0000ff;
                b = (localTemp | 0xffffff00) & 0x0000ff;
                bright = (int) (bright + 0.299 * r + 0.587 * g + 0.114 * b);
            }
        }
        return bright / count;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest
                        .permission.CAMERA}, 100);
            }

        }

        if(activityMainBinding.surface.isAvailable()){
            openCamera(activityMainBinding.surface.getSurfaceTexture());
        } else {
            activityMainBinding.surface.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
                @Override
                public void onSurfaceTextureAvailable(@NonNull SurfaceTexture surfaceTexture, int i, int i1) {
                    openCamera(surfaceTexture);
                }

                @Override
                public void onSurfaceTextureSizeChanged(@NonNull SurfaceTexture surfaceTexture, int i, int i1) {

                }

                @Override
                public boolean onSurfaceTextureDestroyed(@NonNull SurfaceTexture surfaceTexture) {
                    return false;
                }

                @Override
                public void onSurfaceTextureUpdated(@NonNull SurfaceTexture surfaceTexture) {

                }
            });
        }
    }

    private static Bitmap imgToBitmap(Image img, int width, int height) throws IOException {
        byte[] nv21 = YUV_420_888toNV21(img);
        Bitmap bitmap = null;
        YuvImage image = new YuvImage(nv21, ImageFormat.NV21, width, height, null);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        image.compressToJpeg(new Rect(0, 0, width, height), 80, stream);
        bitmap = BitmapFactory.decodeByteArray(stream.toByteArray(), 0, stream.size());
        stream.close();
        return bitmap;
    }

    public Bitmap getChangedBitmap(Bitmap resource,
                                          float hue,
                                          float saturation,
                                          float lum){
        hue = (hue - MID_VALUE) * 1f / MID_VALUE * 180;
        saturation = saturation * 1f / MID_VALUE;
        lum = lum * 1f / MID_VALUE;

        Bitmap out = Bitmap.createBitmap(resource.getWidth()
                ,resource.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(out);
        Paint paint = new Paint();
        paint.setAntiAlias(true);//抗鋸齒

        //調整飽和度
        ColorMatrix saturationMatrix = new ColorMatrix();
        saturationMatrix.setSaturation(saturation);

        //調整色相
        ColorMatrix hueMatrix = new ColorMatrix();
        hueMatrix.setRotate(0,hue);//調整紅像素的色相
        hueMatrix.setRotate(1,hue);
        hueMatrix.setRotate(2,hue);

        //調整亮度
        ColorMatrix lumMatrix = new ColorMatrix();
        lumMatrix.setScale(lum,lum,lum,1);

        //把色相/飽和度/明度 合併成一個ColorMatrix
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.postConcat(hueMatrix);
        colorMatrix.postConcat(saturationMatrix);
        colorMatrix.postConcat(lumMatrix);

        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(resource,0,0,paint);
        return out;

    }

    private static byte[] YUV_420_888toNV21(Image image) {

        int width = image.getWidth();
        int height = image.getHeight();
        int ySize = width*height;
        int uvSize = width*height/4;

        byte[] nv21 = new byte[ySize + uvSize*2];

        ByteBuffer yBuffer = image.getPlanes()[0].getBuffer(); // Y
        ByteBuffer uBuffer = image.getPlanes()[1].getBuffer(); // U
        ByteBuffer vBuffer = image.getPlanes()[2].getBuffer(); // V

        int rowStride = image.getPlanes()[0].getRowStride();
        assert(image.getPlanes()[0].getPixelStride() == 1);

        int pos = 0;

        if (rowStride == width) { // likely
            yBuffer.get(nv21, 0, ySize);
            pos += ySize;
        }
        else {
            int yBufferPos = -rowStride; // not an actual position
            for (; pos<ySize; pos+=width) {
                yBufferPos += rowStride;
                yBuffer.position(yBufferPos);
                yBuffer.get(nv21, pos, width);
            }
        }

        rowStride = image.getPlanes()[2].getRowStride();
        int pixelStride = image.getPlanes()[2].getPixelStride();

        assert(rowStride == image.getPlanes()[1].getRowStride());
        assert(pixelStride == image.getPlanes()[1].getPixelStride());

        if (pixelStride == 2 && rowStride == width && uBuffer.get(0) == vBuffer.get(1)) {
            // maybe V an U planes overlap as per NV21, which means vBuffer[1] is alias of uBuffer[0]
            byte savePixel = vBuffer.get(1);
            try {
                vBuffer.put(1, (byte)~savePixel);
                if (uBuffer.get(0) == (byte)~savePixel) {
                    vBuffer.put(1, savePixel);
                    vBuffer.position(0);
                    uBuffer.position(0);
                    vBuffer.get(nv21, ySize, 1);
                    uBuffer.get(nv21, ySize + 1, uBuffer.remaining());

                    return nv21; // shortcut
                }
            }
            catch (ReadOnlyBufferException ex) {
                // unfortunately, we cannot check if vBuffer and uBuffer overlap
            }

            // unfortunately, the check failed. We must save U and V pixel by pixel
            vBuffer.put(1, savePixel);
        }

        // other optimizations could check if (pixelStride == 1) or (pixelStride == 2),
        // but performance gain would be less significant

        for (int row=0; row<height/2; row++) {
            for (int col=0; col<width/2; col++) {
                int vuPos = col*pixelStride + row*rowStride;
                nv21[pos++] = vBuffer.get(vuPos);
                nv21[pos++] = uBuffer.get(vuPos);
            }
        }

        return nv21;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mCameraDevice != null){
            mCameraDevice.close();
            mCameraDevice = null;
        }
        if(imageReader != null){
            imageReader.close();
            imageReader = null;
        }
    }

    public void savePic(Bitmap cropBitmap) {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "title");
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.Images.Media.DISPLAY_NAME, "cropImageView" + UUID.randomUUID().toString());
        Uri uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                values);

        OutputStream outstream;
        try {
            outstream = getContentResolver().openOutputStream(uri);

            cropBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outstream);
            outstream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int getJpegOrientation(CameraCharacteristics c, int deviceOrientation) {
        if (deviceOrientation == android.view.OrientationEventListener.ORIENTATION_UNKNOWN) return 0;
        int sensorOrientation = c.get(CameraCharacteristics.SENSOR_ORIENTATION);

        // Round device orientation to a multiple of 90
        deviceOrientation = (deviceOrientation + 45) / 90 * 90;

        // Reverse device orientation for front-facing cameras
        boolean facingFront = c.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT;
        if (facingFront) deviceOrientation = -deviceOrientation;

        // Calculate desired JPEG orientation relative to camera orientation to make
        // the image upright relative to the device orientation
        int jpegOrientation = (sensorOrientation + deviceOrientation + 360) % 360;

        return jpegOrientation;
    }


    public static Bitmap convertToBlackWhite(Bitmap bmp,int changeLight) {
        int width = bmp.getWidth(); // 獲取點陣圖的寬
        int height = bmp.getHeight(); // 獲取點陣圖的高
        int[] pixels = new int[width * height]; // 通過點陣圖的大小建立畫素點陣列

        bmp.getPixels(pixels, 0, width, 0, 0, width, height);
        int tmp =120;
        if(changeLight == 180){
            tmp = 60;
        }

//        if(changeLight == 90){
//            tmp = 120;
//        }


        int alpha = 0xFF << 24;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                int grey = pixels[width * i + j];

                int red = ((grey & 0x00FF0000) >> 16);
                int green = ((grey & 0x0000FF00) >> 8);
                int blue = (grey & 0x000000FF);


                if (red > tmp) {
                    red = 255;
                } else {
                    red = 0;
                }
                if (blue > tmp) {
                    blue = 255;
                } else {
                    blue = 0;
                }
                if (green > tmp) {
                    green = 255;
                } else {
                    green = 0;
                }

                pixels[width * i + j] = (alpha | red |green| blue);

                grey =(int)((red+green+blue)/3);

//                grey = (int) (red * 0.3 + green * 0.59 + blue * 0.11);
                grey = alpha | (grey << 16) | (grey << 8) | grey;


//                Log.e("wtf","   "+grey);
                pixels[width * i + j] = grey;

//                if (pixels[width * i + j] == -1) {
//                    pixels[width * i + j] = -1;
//                } else {
//                    pixels[width * i + j] = -16777216;
//                }
            }
        }
        Bitmap newBmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);

        newBmp.setPixels(pixels, 0, width, 0, 0, width, height);

        return newBmp;
    }

}