package com.example.mymlkit;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyRequest {
    public static void send(Context context,String result) throws JSONException {
        JSONObject params = new JSONObject();
        params.put("signedAttestation",result);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, "https://www.googleapis.com/androidcheck/v1/attestations/verify?key=AIzaSyDQEWqW1L7lUVlrXtMdunSGexb5Htxd4aI", params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson = new Gson();
                        Log.e("wtf","success  "+gson.toJson(response));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Gson gson = new Gson();
                Log.e("wtf","error  "+gson.toJson(error));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return new HashMap<>();
            }
        };

        RequestQueue queue = Volley.newRequestQueue(context);
        queue.add(request);
    }
}
